class Post < ActiveRecord::Base
  belongs_to :blog
  validates_formatting_of :url, using: :url
end
