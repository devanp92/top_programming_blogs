class Blog < ActiveRecord::Base
  has_many :posts
  validates_formatting_of :url, using: :url
end
