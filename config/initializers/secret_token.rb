# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
TopProgrammingBlogs::Application.config.secret_key_base = '519f307620e321725b923db26b5bfbff907cac15fef595c1e9bea219def8310c4d0f1368148ef6090530694d81ba7360637e7615acdbed8d0e9f370f6722abb8'
