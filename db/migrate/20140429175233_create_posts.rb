class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :post
      t.string :url
      t.references :blog, index: true

      t.timestamps
    end
  end
end
